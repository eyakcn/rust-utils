//#![feature(use_extern_macros)] 从 1.30.0 不再需要手动引入

extern crate wasm_bindgen;

use wasm_bindgen::prelude::*;

#[wasm_bindgen(module = "domUtils")]
extern {
  fn appendNumToBody(x: u32);
  fn appendStrToBody(s: &str);
//  fn alert(x: u32);
}

#[wasm_bindgen]
pub fn run() {
  appendNumToBody(42);
  appendStrToBody("Hello World"); // 仅支持数字，所以输出 1048576
//    alert(4);
}

#[wasm_bindgen]
pub fn add_one(x: u32) -> u32 {
  x + 1
}

//#[no_mangle]
//pub extern fn run() {
//  unsafe {
//    appendNumToBody(42);
//    appendStrToBody("Hello World"); // 仅支持数字，所以输出 1048576
////    alert(4);
//  }
//}

//#[no_mangle]
//pub extern fn add_one(x: u32) -> u32 {
//  x + 1
//}